### Ubicarse en carpeta para crear imagen

`docker build -t mail-postfix:0.0.1 .`

### Verificar imagen creada

`docker images`

### Crear contenedor de imagen

`docker run -d --name postfix -p 7777:80 mail-postfix:0.0.1`

### Verificar contenedor creado

`docker ps -a`

### Acceder a contenedor

`docker exec -it <containerId> bash`

### Validar envio
Ingresar a `http://127.0.0.1:7777/emaker.php` en navegador y verificar que todo esté correcto

### Ver logs de postfix

`cat /var/log/mail.log`

### Ver logs de apache
`cat /var/log/apache2/error.log`