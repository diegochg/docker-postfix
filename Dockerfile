FROM ubuntu:18.04
EXPOSE 80

RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
RUN apt-get update && apt-get install -y apt-utils 2>&1 | grep -v "^debconf: delaying package configuration, since apt-utils.*" && \
    apt-get install -y libsasl2-modules postfix mailutils rsyslog apache2 php libapache2-mod-php; rm -rf /var/lib/apt/lists/* && \
    rm -rf /var/cache/apt/* && \ 
    sed -i -e 's/inet_interfaces = localhost/inet_interfaces = all/g' /etc/postfix/main.cf

RUN sed -i -e 's/www-data/postfix/g' /etc/apache2/envvars

COPY emaker.php /var/www/html
COPY sendmail.sh /var/www/html
COPY run.sh /
RUN chmod +x /run.sh

CMD ["/run.sh"]
RUN echo "/etc/init.d/apache2 start \n /etc/init.d/rsyslog start \n etc/init.d/postfix restart" >> /root/.bashrc
