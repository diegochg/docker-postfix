#!/bin/bash
postconf -e "myhostname = tedregal.com"
postconf -e "mydestination = localhost.tedregal.com, localhost, localhost.localdomain"
postconf -e "myorigin = localhost"
postconf -e "relayhost = [smtp-relay.sendinblue.com]:587"
postconf -e "smtp_sasl_auth_enable = yes"
postconf -e "smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd"
postconf -e "smtp_sasl_security_options = noanonymous"
postconf -e "smtp_tls_security_level = may"
postconf -e "header_size_limit = 4096000"
postconf -e "mynetworks = 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128"
postconf -e "inet_protocols = ipv4"

echo "[smtp-relay.sendinblue.com]:587 diego.chavez@tedregal.com:dxC2g05crWM3BULI" >> /etc/postfix/sasl_passwd
postmap /etc/postfix/sasl_passwd

exec /usr/sbin/postfix -c /etc/postfix start-fg
