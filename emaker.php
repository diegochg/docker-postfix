<?php
class Mail {

  private $from;
  private $to;
  private $cc;
  private $subject;
  private $body;
  
  public function __construct($data) {
    $this->from = $data['from'];
    $this->to = $data['to'];
    $this->cc = $data['cc'];
    $this->subject = $data['subject'];
    $this->body = $data['body'];
  }

  public function sendMail() {
    $variables = "" . $this->from . " ". implode(',',$this->to) . " ". implode(',',$this->cc) . " '$this->subject'" . " '$this->body'";
    shell_exec("/var/www/html/sendmail.sh " . $variables);
    return "Mail sent successfully";
  }

}

header('Content-Type: application/json');
$response = array();
$data = json_decode(file_get_contents("php://input"), true);
$mail = new Mail($data);
$response["Response"] = $mail->sendMail();
echo json_encode($response);
?>
